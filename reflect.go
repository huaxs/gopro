package main

import (
	"fmt"
	"reflect"
)

type order struct {
	oldId int
	customerId int
}

type employee struct {
	name string
	id int
	address string
	salary int
	country string
}

func main() {
	o := order{
		oldId: 456,
		customerId: 56,
	}
	createQuery(o)

	e := employee{
		name: "Naveen",
		id: 565,
		address: "Coimbatore",
		salary: 90000,
		country: "India",
	}
	createQuery(e)

	i:= 90
	createQuery(i)
}

func createQuery(q interface{})  {
	x := reflect.TypeOf(q)
	y := reflect.ValueOf(q)

	switch x.Kind() {
	case reflect.Struct:
		str := fmt.Sprint(`insert into `+ x.Name()+` values(` )
		for i := 0; i < y.NumField(); i++ {
			if y.Field(i).Kind() == reflect.Int {
				str +=fmt.Sprint( y.Field(i).Int())
			} else if y.Field(i).Kind() == reflect.String {
				str += fmt.Sprint( `"`+y.Field(i).String()+`"`)
			}
			if i != y.NumField()-1 {
				str += ","
			}
		}
		str += ")"
		fmt.Println(str)
	default:
		fmt.Println("unsupported type")
	}
}